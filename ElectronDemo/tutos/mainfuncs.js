// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcRenderer, globalShortcut,dialog} 
= require('electron')
require("./tutos/topMenu.js/index.js")
require("./tutos/contextMenu.js")
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    //transparent: true,
    //frame: true
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  mainWindow.on('click', () => {
    ipcRenderer.send('show-context-menu')
  })
  // fait apparaitre une fenetre avec options
  mainWindow.on('crashed', function () {
    const options = {
      type: 'info',
      title: 'Appli qui crash',
      message: 'L\'appli s\'est bloquée',
      buttons: ['Slap him','Kill this app'],
    }
    dialog.showMessageBox(options, (index) => {
      if(index === 0) mainWindow.reload()
      else mainWindow.close()
    })
  })
  // fait apparaitre une fenetre avec options
  mainWindow.on('unresponsive', function () {
    const options = {
      type: 'info',
      title: 'Appli qui répond pas',
      message: 'L\'appli s\'est endormie',
      buttons: ['Slap him','Kill this app'],
    }
    dialog.showMessageBox(options, (index) => {
      if(index === 0) mainWindow.reload()
      else mainWindow.close()
    })
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
//app.on('ready', createWindow)

app.on('ready', () => {
  createWindow()
  //raccourci global
  globalShortcut.register('CommandOrControl+Shift+K', () => {
    dialog.showMessageBox({
      type: 'none', //ne pas changer sinon erreur
      message: 'Wow, amazing',
      detail: 'Tu as appuyé au hasard et te voilà ici !!',
      buttons: ['kay','stupid']
    })
  })
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    //suppr les raccourcis globaux
    globalShortcut.unregisterAll()
    app.quit()
    //createWindow()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
