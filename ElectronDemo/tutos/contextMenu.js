const {
  BrowserWindow,
  Menu,
  MenuItem,
  ipcMain,
  dialog,
  app
} = require('electron')
const path = require('path')

//customisation du clic droit
const menu = new Menu()
menu.append(new MenuItem({ label: 'Yolo' }))
menu.append(new MenuItem({ type: 'separator' }))
menu.append(new MenuItem({ label: 'Zbobiton', type: 'checkbox', checked: true }))

app.on('browser-window-created', (event, win) => {
  win.webContents.on('context-menu', (e, params) => {
    menu.popup(win, params.x, params.y)
  })
})

ipcMain.on('show-context-menu', (event) => {
  const win = BrowserWindow.fromWebContents(event.sender)
  menu.popup(win)
})
//réceptionne l'ipcRenderer dans l'index pour exécuter une commande
ipcMain.on('button-clicking-main',(event,argu) => {
  const win = BrowserWindow.fromWebContents(event.sender)
  if(argu === 'reload') win.reload()
  else win.close()
})
//permet d'ouvrir un explorer de selection de fichier/dossier
ipcMain.on('open-file-dialog', (event) => {
  dialog.showOpenDialog({
    properties: ['openFile'] //ajouter element openDirectory pour ouvrir un dossier
  }, (files) => {
    if (files) {
      event.sender.send('selected-directory', files)
    }
  })
})
/* Liste des dialog :
showErrorDialog('titre','description')
showMessageBox(options[type,title,message,buttons], function(index)=>{})
showSaveDialog(options[title,filters[{name,extnsions}]], function(filename)=>{})
*/

//pas convaincu de cette feature, peut être utiliser html5 drag & drop
ipcMain.on('ondragstart', (event, filepath) => {
  const iconName = 'codeIcon.png'
  event.sender.startDrag({
    file: filepath,
    icon: path.join(__dirname, iconName)
  })
})