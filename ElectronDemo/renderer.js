// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

module.exports = {

  rightPartToggle: function () {
    var leftPart = document.getElementsByClassName('column leftPart');
    var rightPart = document.getElementsByClassName('column rightPart is-half');
    if (rightPart.length > 0) {
      rightPart[0].parentNode.removeChild(rightPart[0]);
      leftPart[0].classList.remove('is-half');
      leftPart[0].classList.add('is-full');
    } else {
      var newPart = document.createElement('div');
      newPart.className = "column rightPart is-half";
      newPart.style.backgroundColor = 'rgb(200, 200, 200)';
      leftPart[0].parentNode.insertBefore(newPart, leftPart[0].nextSibling);
      leftPart[0].classList.remove('is-full');
      leftPart[0].classList.add('is-half');
    }
  },

  createMailTable: function () {
    //module.exports.rightPartToggle();
    const fs = require('fs');
    var rightPart = document.getElementsByClassName('column rightPart');
    var mail = document.getElementsByClassName('mailCreate')
    if (rightPart.length > 0) {
      if (mail.length > 0) {
        mail[0].parentNode.removeChild(mail[0]);
      }
      var nouveauMail = document.createElement('div');
      nouveauMail.className = "mailCreate";
      fs.readFile('ressources/newMail.html', (err, data) => {
        nouveauMail.innerHTML = data;
      })
      document.getElementsByClassName('column rightPart')[0].appendChild(nouveauMail);
    }
  },

  submitting: function () {
    const ipc = require('electron').ipcRenderer
    var destiMail = document.getElementsByClassName('destiMail');
    var objetMail = document.getElementsByClassName('objetMail');
    var contenuMail = document.getElementsByClassName('contenuMail');
    if (destiMail.length > 0) {
      if (objetMail.length > 0) {
        if (contenuMail.length > 0) {
          var jsonMail = {
            "destination": destiMail[0].value,
            "entete": objetMail[0].value,
            "contenu": contenuMail[0].value,
            "type": "mail"
          }
          alert('message crée');
          ipc.send('createMail', jsonMail);
        }
      }
    }
  },

  canceling: function () {
    var rightPart = document.getElementsByClassName('column rightPart is-half');
    if (rightPart.length > 0) {
      rightPart[0].parentNode.removeChild(rightPart[0]);
    }
  },

  openWebMail: function () {
    const ipc = require('electron').ipcRenderer
    ipc.send('openWebMail');
  },

  openMail: function (filePath) {
    const ipc = require('electron').ipcRenderer
    ipc.send('openMail');
  },

  readMail: function (mailToRead) {
    const ipc = require('electron').ipcRenderer
    var rightPart = document.getElementsByClassName('column rightPart');
    var mailList = document.getElementsByClassName('mailList');
    rightPart.innerHTML = "";
    //vérifier si partie droite existe avant
    //alert(mailToRead);
    //console.log("reading mails");
    ipc.send('readMail', mailToRead)
  }
}


