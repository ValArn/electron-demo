// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain, dialog }
  = require('electron')
const fs = require('fs')
var configPath = app.getAppPath() + '/ressources/mailfolder/mailConfig.json';
var configFile = fs.readFileSync(configPath);
var config = JSON.parse(configFile);
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    //transparent: true,
    //frame: true
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  mainWindow.on('click', () => {
    ipcRenderer.send('show-context-menu')
  })
}

ipcMain.on('createMail', (event, arg) => {
  var path = app.getAppPath() + '/ressources/mailfolder/mail' + config["mailNbr"] + '.json';
  fs.writeFile(path, JSON.stringify(arg), (err) => {
    if (err) throw err;
    config["mailNbr"] += 1;
    fs.writeFileSync(configPath, JSON.stringify(config));
    event.sender.send('createdMail', path);
  });
})

ipcMain.on('openMail', (event) => {
  dialog.showOpenDialog({
    properties: ['openFile'] //ajouter element openDirectory pour ouvrir un dossier
  }, (files) => {
    if (files) {
      event.sender.send('selectedMail', files)
    }
  })
})

ipcMain.on('openWebMail', (event) => {
  var webWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
    },
    //transparent: true,
    //frame: true
  })
  // and load the index.html of the app.
  webWindow.loadURL('https://google.com/');
  // Emitted when the window is closed.
  webWindow.on('closed', function () {
    webWindow = null
  })
})

ipcMain.on('readMail', (event, mailName) => {
  //besoin du chemin de fichier
  var regMail = new RegExp(mailName, "g");
  var mailPath = "not found";
  var readed = false;
  fs.readdir('./ressources/mailFolder', (err, dir) => {
    var done = false;
    dir.forEach(el => {
      if (el.match(regMail) && !done) {
        mailPath = app.getAppPath() + '/ressources/mailfolder/' + el;
        //console.log("yolo doneed " + mailPath);
        done = true;
      }
    });
    readed = true;
    //à tester
    if(readed) {
      //console.log("you're done " + mailPath);
      if (mailPath != "not found") {
        mailContent = fs.readFileSync(mailPath);
        //console.log("dooming " + typeof(mailContent));
        event.sender.send('readingMail', mailContent);
      } else {
        event.sender.send('readingMail', "{ \"type\": \"not found\" }");
      }
    }
  });
})
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
//app.on('ready', createWindow)

app.on('ready', () => {
  createWindow()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    //suppr les raccourcis globaux
    app.quit()
    //createWindow()
  }
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
