const { app, BrowserWindow, ipcMain, dialog } = require('electron')
const routes = require('./ressources/helpers/routes') //fichier de routes
const appMenu = require('./ressources/helpers/appMenu') //fichier de routes
let mainWindow

function createWindow() {

  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    //transparent: true,
    //frame: true
  })

  mainWindow.loadFile('index.html')

  mainWindow.on('closed', function () {

    mainWindow = null
  })

  mainWindow.on('click', () => {
    ipcRenderer.send('show-context-menu')
  })
}

app.on('ready', () => {
  routes.routes()
  appMenu.initMenu()
  //utiliser les ipc comme des appels de routes
  createWindow()
})

app.on('window-all-closed', function () {

  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {

  if (mainWindow === null) {
    createWindow()
  }
})
