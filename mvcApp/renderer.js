// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const ipc = require('electron').ipcRenderer

module.exports = {
  sending : (route,argument) => {
    ipc.send(route,argument)
  },
  routes : () => {
    ipc.on('fool', (event,arg) => {
      console.log(arg)
    })
  }
}