const {app,BrowserWindow} = require('electron')


class Vue {
  constructor() {}
  accueil() {
    var focusedView = BrowserWindow.getFocusedWindow()
    focusedView.loadFile(app.getAppPath() + '/ressources/views/accueil.html')
  }

  mainPage() {
    var focusedView = BrowserWindow.getFocusedWindow()
    focusedView.loadFile(app.getAppPath() + '/index.html')
  }

  google() {
    //console.log('blyet')
    var focusedView = BrowserWindow.getFocusedWindow()
    focusedView.loadURL('https://www.google.com')
  }
}

module.exports = Vue