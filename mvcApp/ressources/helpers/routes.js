const app = require('electron').app
const ipc = require('electron').ipcMain

module.exports = {
   
  routes : () => {
    ipc.on('yolo', (event,arg) => {
      console.log('yolo')
    })

    ipc.on('firewatch', (event,arg) => {
      console.log('fireblayme')
      event.sender.send('fool','blayme')
    })

    ipc.on('chose2', (event,arg) => {
      const Chose2 = require(app.getAppPath() + '/ressources/controllers/chose2.js')
      var obj2 = new Chose2('arguObj')
      switch (arg) {
        case "voir":
          obj2.affichage()
          break;
      
        default:
          break;
      }
    })

    ipc.on('vue', (event,arg) => {
      const Vue = require(app.getAppPath() + '/ressources/controllers/vue.js')
      var vue = new Vue()
      switch (arg) {
        case 'accueil':
          vue.accueil()
          break;
        
        case 'google':
          vue.google()
          break;
        
        case 'mainPage':
          vue.mainPage()
          break;
      
        default:
          break;
      }
    })

    ipc.on('navigation', (event,arg) => {
      //faire revenir au début de l'app
    })
  }

}